import asyncio
import sdp_transform


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        udp = data.decode()
        print('Received %r from %s' % (udp, addr))
        udp = sdp_transform.parse(udp)
        udp['media'][0]["port"] = 34543
        udp['media'][1]["port"] = 34543
        udp = sdp_transform.write(udp)
        print('Send %r to %s' % (udp, addr))
        self.transport.sendto(udp.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
