import asyncio


async def hello():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')


async def main():
    await asyncio.gather(hello(), hello(), hello())

asyncio.run(main())
